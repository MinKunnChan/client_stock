import * as TYPES from "./types";

export const updateStocks = data => dispatch => {
	dispatch({ type: TYPES.STOCKS, payload: data });
};

export const updateStompClient = data => dispatch => {
	dispatch({ type: TYPES.STOMP_CLIENT, payload: data });
};
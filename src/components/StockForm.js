import React, { Component } from "react";
import { connect } from "react-redux";

export class StockForm extends Component {

    state ={
    }

    handleFormSubmit = e => {
        e.preventDefault();
      
        let stock = {
            symbol: this.symbol.value,
            ask : this.ask.value,
            bid: this.bid.value
        }
        try {
            this.props.stompClient.send("/app/stock", {}, JSON.stringify(stock));
            alert("Successfully updated new stock price! See the stocks pages");
        } catch (e) {
            console.log(e.message);
            alert("trying connect to server! plase wait a while");
        }
    }

	render() {
		return (
			<div className="container">
                <h4 className="alert-heading">Stock Form</h4> 
                <form onSubmit={this.handleFormSubmit}>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Symbol</label>
                        <div className="col-sm-10">
                        <input type="text" ref={input => this.symbol = input} required={true} className="form-control" />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Bid Price</label>
                        <div className="col-sm-10">
                        <input type="number" required={true} className="form-control" ref={input => this.bid = input}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Ask Price</label>
                        <div className="col-sm-10">
                        <input type="number" required={true} className="form-control" ref={input => this.ask = input}/>
                        </div>
                    </div>
                    <div className="form-group row">
                    <label className="col-sm-2"></label>
                        <div className="col-sm-10">
                            <button type="submit" className="btn btn-success">Add</button>
                        </div>
                    </div>
                </form>
            </div>
		);
	}
}
const mapStateToProps = ({ stocks, stompClient }) => {
	return { stocks, stompClient };
};

export default connect(mapStateToProps)(StockForm);

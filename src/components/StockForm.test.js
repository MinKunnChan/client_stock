import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import TestUtils from 'react-dom/test-utils';
import ReactDOM from 'react-dom';
import { StockForm } from './StockForm'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Stock Form', () => {
   
    it('should renders a submit button', () => {
        const wrapper = shallow(<StockForm />)
        const button = wrapper.find('button[type="submit"]')
        expect(button).toHaveLength(1)
        expect(button.text()).toEqual('Add')
    });

    it('should render form elements', () => {
        const wrapper = shallow(<StockForm />)
        const inputText = wrapper.find('input[type="text"]')
        const inputNumber = wrapper.find('input[type="number"]')
        expect(inputText).toHaveLength(1)
        expect(inputNumber).toHaveLength(2)
    })
  })
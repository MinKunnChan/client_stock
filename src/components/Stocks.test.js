import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Stocks } from './Stocks'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Stocks', () => {
   
    it('should renders a link', () => {
        const wrapper = shallow(<Stocks stocks={[]} />)
        const link = wrapper.find('Link')
        expect(link).toHaveLength(1)
        expect(link.text()).toEqual('Add Stock')
    });

    it('should render stocks with data', () => {
        const data = [{symbol: "A", price : "23", trend : "UP"}]
        const wrapper = shallow(<Stocks stocks={data} />)
        const stocks = wrapper.find('tbody tr')
        expect(stocks).toHaveLength(data.length)
        expect(stocks.first().text()).toEqual('A23UP')
    })
  
  })
import React from 'react';

function Header({connected}){
    
    return(
        <div>
            <div className="alert alert-success text-center" role="alert">
                <h4 className="alert-heading">Stock Price on Real-time</h4>
            </div>
            {
                connected ? null : (
                    <div className="alert alert-light loading" role="alert">
                        <h5 className="alert-heading">Connecting to real-time Api...</h5>
                    </div>
                )
            }
           
        </div>
    )
}

export default Header;
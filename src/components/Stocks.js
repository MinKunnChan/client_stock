import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export class Stocks extends Component {

	render() {
        const stocks = this.props.stocks ? this.props.stocks : [];
		return (
            <div className="container">
                <div className="col-md-12">
                    <h4 className="alert-heading">Stocks</h4> 
                    <div className="mb-3">
                        <Link to="/add"><button type="button" className="btn btn-success">Add Stock</button></Link>
                    </div>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Symbol</th>
                            <th>Price</th>
                            <th>Trend</th>
                        </tr>
                        </thead>
                        <tbody>
                            {
                                stocks.map((stock, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{stock.symbol}</td>
                                        <td>{stock.price}</td>
                                        <td>{stock.trend}</td>
                                    </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
		);
	}
}
const mapStateToProps = ({ stocks }) => {
	return { stocks };
};

export default connect( mapStateToProps)(Stocks);

import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Header from './Header'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Header', () => {
  it('should render header', () => {
    const wrapper = shallow(<Header connected={false} />)
    const h4 = wrapper.find('h4')
    expect(h4).toHaveLength(1)
    expect(h4.props().children).toEqual('Stock Price on Real-time')

    const h5 = wrapper.find('h5')
    expect(h5).toHaveLength(1)
    expect(h5.props().children).toEqual('Connecting to real-time Api...')
  })

  it('should render header without loading state', () => {
    const wrapper = shallow(<Header connected={true} />)
    const div = wrapper.find('div[className="loading"]')
    expect(div).toHaveLength(0)
  })
})
import { combineReducers } from "redux";
import stockReducer from "./stockReducer";
import stompReducer from "./stompReducer";

export default combineReducers({
	stocks: stockReducer,
	stompClient: stompReducer
});

import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import './App.css';
import { connect } from "react-redux";
import { updateStompClient, updateStocks } from "./actions";
import SockJS from 'sockjs-client';
import Stomp from "stompjs";
import Stocks from './components/Stocks';
import StockForm from './components/StockForm';
import Header from './components/Header';


class App extends Component {

  state = {
    connected: false
  }

  componentDidMount = () =>{
    const self = this;
    let socket = new SockJS('/stock-socket');
    let stompClient = Stomp.over(socket);
    let stock = {
      symbol: "",
      ask : 0,
      bid: 0
    }
    stompClient.connect({}, function (frame) {
        self.setState({connected : true})
        stompClient.subscribe('/topic/stocks', function (respond) {
          const body = JSON.parse(respond.body);
          self.props.updateStocks(body.stocks)
        });
        stompClient.send("/app/stock", {}, JSON.stringify(stock));

    });
    
    
    self.props.updateStompClient(stompClient);
  }

  componentWillUnmount = () =>{
    
    const { stompClient } = this.props;
    if (stompClient !== null) {
      stompClient.disconnect();
    }
    console.log("Disconnected");
  }

  render(){
   
    return (
      <BrowserRouter>
        <div>
          <Header connected={this.state.connected} />
          <Switch>
            <Route exact path="/" component={Stocks} />
            <Route exact path="/add" component={StockForm} />
            <Redirect to="/" />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = ({ stompClient }) => {
	return { stompClient };
};
export default connect( mapStateToProps, { updateStompClient, updateStocks })(App);

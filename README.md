## Real-time stock price / Client

 by Min Kunn Chan, June 30 2020

## Client Setup 

This project is using ReactJs technology with SockJS and redux, redux-thunk

## Steps to run
1. clone the project and navigate to it
2. run command => npm clean install
3. run command => npm run start
4. once the application you can go to http://localhost:3000 for main page(Stocks page)
5. open new browser tab or new browser window and access to http://localhost:3000/add
6. insert or update new stock price and you can see the stocks values are changed according to your data (without reloading the page)

#### Note
If there are any issue email me @ kunnchan3@gmail.com or call me @ 98680039